<h4>
    &nbsp; &nbsp;
	介绍
</h4>
<p>
    &nbsp; &nbsp;
	一个spring boot+maven+uniapp 的JAVA开源项目，前后端分离、&nbsp; 小程序 + APP + 公众号，是一套拥有完整的流程设计精美的小程序设计。
</p>
<h4>
    &nbsp; &nbsp;
	使用说明
</h4>
<ol style="margin-left:20px;" class=" list-paddingleft-2">
    <li>
        <p>
            &nbsp;本项目开源的目的是让每个人轻松搭建自己，轻松上手，但是您还是有一些必要的环境准备，在运行本项目之前，您必须完成以下的步骤。
        </p>
    </li>
    <li>
        <p>
            &nbsp; 环境要求：为了避免运行开发中遇到一些意想不到的问题，我们推荐你安装如下环境。Git Node9+ hbuilderX 微信开发者工具
        </p>
    </li>
    <li>
        <p>
            <strong>本项目目前还未完善，本项目会持续的更新。</strong><br/>
        </p>
    </li>
    <li>
        <p>
            <strong>本项目想合作或购买源码，详情咨询微信：xinfeng0921</strong>
        </p>
    </li>
</ol>
<h4>
    &nbsp; &nbsp;
	安装教程
</h4>
<ol style="margin-left:20px;" class=" list-paddingleft-2">
    <li>
        <p>
            克隆本项目到本地
        </p>
    </li>
    <li>
        <p>
            配置hbuilderX 该项目推荐使用hbuilderX调试编写，在使用之前请进行必要的配置：node和微信开发者工具。
        </p>
    </li>
    <li>
        <p>
            &nbsp; &nbsp; &nbsp;开始使用 打开hbuilderX导入项目，运行-&gt;运行到手机或模拟器（在手机端调试）。
        </p>
    </li>
</ol>
<h4>
    &nbsp; &nbsp;
	技术说明
</h4>
<p>
    <br/>
</p>
<table>
    <tbody>
        <tr class="firstRow">
            <th style="text-align:center;background-color:#F1F1F1;">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				框架
            </th>
            <th style="text-align:center;background-color:#F1F1F1;">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				说明
            </th>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				vue
            </td>
            <td>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				基础的代码编写
            </td>
        </tr>
        <tr>
            <td>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				uniapp
            </td>
            <td>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				跨端方案
            </td>
        </tr>
    </tbody>
</table>
<p style="text-align: left;">
    项目演示
</p>
<p>
    <br/>
</p>
<p>
    <img src="https://shopimges.oss-cn-hangzhou.aliyuncs.com/mimi/xwx.jpg"/>
</p>
<p>
    <br/>微信联系我
</p>
<p>
    <img src="http://zhaoimge.oss-cn-shenzhen.aliyuncs.com/imges/1639551574442.jpg"/>
</p>
<p>
    <br/>
</p>
<p>
    <br/>
</p>
<p>
    <br/>
</p>